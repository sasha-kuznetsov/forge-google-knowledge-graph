# Confluence Google Knowledge Graph API App

This app is an integration between the [Google Knowledge Graph API](https://developers.google.com/knowledge-graph) and Confluence using [Atlassian Forge](https://developer.atlassian.com/platform/forge/)

## Usage

To use it first select text in the wiki for an entity that you would like more information about (People, Places, Products, Etc.)

![Selected Text](https://i.imgur.com/NhGRhNt.png)

Next Select the app from the menu and then more information will be displayed in a tooltip

![Tooltip Example](https://i.imgur.com/j4KSy1g.png)

## Installation

- Follow the directions to get a [API key](https://developers.google.com/knowledge-graph/how-tos/authorizing) for Google Knowledge Graph,
- Add it in src/index.js
- Run `forge deploy`
